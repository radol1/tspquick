import sys, argparse

# process a cvs file containing TSP transactions

# define ticker to name mapping
ticker_fund_map = {
    "TSPGF": "TSP G Fund",
    "TSPCF": "TSP C Fund",
    "TSPSF": "TSP S Fund",
    "TSPIF": "TSP I Fund",
    "TSPFF": "TSP F Fund",
    "TSPLIF": "TSP L Income Fund",
    "TSPL20F": "TSP L 2020 Fund",
    "TSPL30F": "TSP L 2030 Fund",
    "TSPL40F": "TSP L 2040 Fund",
    "TSPL50F": "TSP L 2050 Fund"
    }
qentries = "^\n!Type:Invst\n"

# Create qif contribution entries
def create_qif_contribution_entries (date, memo, ec, er, aac, amc) :
    ent = "XIn"
    ent += "\nD"+date
    ent += "\nT"+ec
    ent += "\nPEmployee Contribution"
    ent += "\n^\n"
    if er and float (er) > 0 :
        ent +="XIn"
        ent += "\nD"+date
        ent += "\nT"+ str(er)
        ent += "\nPEmployee Roth Contribution"
        ent += "\n^\n"
    ent +="XIn"
    ent += "\nD"+date
    ent += "\nT"+aac
    ent += "\nPAgency Automatic Contribution"
    ent += "\n^\n"
    ent +="XIn"
    ent += "\nD"+date
    ent += "\nT"+amc
    ent += "\nPAgency Matching Contribution"
    ent += "\n^\n"
    return ent

# process employee contributions
# The Quarterly statement has the following format
# Payroll Office, Posting Date,Transaction Type,Employee Traditional, Employee Roth,Agency Automatic (1%),Agency Matching, Total
# We expect the Payroll Office to be null and we ignore the Total 
def process_contribution (s) :
    ce = ""
    date = s[1].strip()
    memo = s[2].strip()
    ec = s[3].strip()
    er = s[4].strip()
    aac = s[5].strip()
    amc = s[6].strip()
    #print("date: "+date+", ec:"+ec+", aac:"+aac+", amc:"+amc)
    if memo.find("Contribution") >= 0 :
        ce=create_qif_contribution_entries (date, memo, ec, er, aac, amc)
    #print (ce)
    global qentries
    qentries += ce

# qif transaction entries
def create_qif_transaction_entries (ticker,date,memo,tat,tar,sp,ns) :
    ent = "D"+date
    ent += "\nNBuy"
    ent += "\nY"+ticker_fund_map[ticker]
    ent += "\nQ"+ns
    ent += "\nI"+sp
    ent +="\nT"+tat
    ent += "\nMEmployee Traditional"
    ent += "\n^\n"
    return ent

# write the qif file
def write_qif_file(fw) :
    f = open(fw,"w")
    global qentries
    f.write(qentries)
    f.close()

# process transactions
# We expect the transactions to be in the following format -
# Ticker,Posting Date,Transaction Type,Transaction Amount Traditional, Transaction Amount Roth,Total,Share Price,Number of Shares, Dollar Balance
# We only process "Contribution" Transaction Type (for now) and ignore Roth and Dollar Balance
def process_transactions (s) :
    ticker = s[0].strip()
    date = s[1].strip()
    memo = s[2].strip()
    tat = s[3].strip()
    tar = s[4].strip()
    tot = s[5].strip
    sp = s[6].strip()
    ns = s[7].strip()
    if memo.find("Contribution") >= 0 :
        te=create_qif_transaction_entries(ticker,date,memo,tat,tar,sp,ns)
        global qentries
        qentries += te

# Initiate the parser
parser = argparse.ArgumentParser()

# Add long and short argument for file names
parser.add_argument("-infile", "-i", help="set input filename")
parser.add_argument("--outfile", "-o", help="set output filename")

# Read arguments from the command line
args = parser.parse_args()

if args.infile:
    print("input file " + args.infile)
if args.outfile is None :
    args.outfile= args.infile.replace ("csv", "qif")
    print("output file " + args.outfile)

# open file and iterate through it a line at a time
with open (args.infile,  "r") as f:
    for x in f :
        #print (x)
        s = x.split(",")
        #print (len(s))
        if s[0] == "" :
            #print("contribution")
            process_contribution(s)
        else :
            process_transactions(s)
#print (qentries)
write_qif_file(args.outfile)
f.close()
