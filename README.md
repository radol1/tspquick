**Introduction:**

This program parses a comma seperated values (CSV) file and generates a  qif file to import TSP transactions into Quicken.


**Responsibility:**

It is simple. This program is offered for free. I assume no responsibility.

 
**Quicken Backup:**

Before you use this program, you must back up your Quicken data. It will allow  Quicken to be restored in case of any corruption during transaction import.


**Import TSP Securities:**

The following TSP funds are included in the TSPFundList.qif file and should be imported into Quicken.

```
> Ticker     Fund Name
> 
> "TSPGF": "TSP G Fund"
> "TSPCF": "TSP C Fund"
> "TSPSF": "TSP S Fund"
> "TSPIF": "TSP I Fund"
> "TSPFF": "TSP F Fund"
> "TSPLIF": "TSP L Income Fund"
> "TSPL20F": "TSP L 2020 Fund"
> "TSPL30F": "TSP L 2030 Fund"
> "TSPL40F": "TSP L 2040 Fund"
> "TSPL50F": "TSP L 2050 Fund"
```

To import these securities into Quicken: 

* download TSPFundList.qif file
* In Quicken, Click on File -> File Import -> QIF File.
* In the popup dialog window
    *   select the location of TSPFundList.qif file using the Browse option
    *   select All Accounts for Quicken account to import into
    *   check only the Security Import option for Include in Import
    *   Click Import


**Create a Comma Separated Values (CSV) File:**

You will need to create a csv file of TSP transactions for importing them into Quicken. You can use any text editor like notepad to create this file. If you use MS Excel, select the CSV file type when saving.

*Helpful notes:*

* The support for Roth contributions in TSP began in 2Q 2012. THe quarterly statements added an extra column for employee contributions and two columns for fund purchases. I adhere to this new format for the csv file. You will have to include empty placeholder(s) by adding an extra blank column(s) for contributions and transactions prior to 2Q 2012.
* I have found it to be easy to open the quarterly TSP statements in MS Word and then select and copy transactions into either MS Excel or notepad.
* I create these csv files only for one or two quarterly statements. This makes verification of imported transactions easier.

The expected format for contributions is:

`Payroll Office,Posting Date,Transaction Type,Employee Traditional,Employee Roth,Agency Automatic (1%),Agency Matching,Total`

*Important Notes:*

The first column for Payroll Office is expected not to have any value in it. You will see that is a "," in the first column without any values in the examples below.

The expected format for fund transactions is:

`Ticker,Posting Date,Transaction Type,Transaction Amount Traditional,Transaction Amount Roth,Transaction Amount Total,Share Price,Number of Shares,Dollar Balance`

*Important Notes:*

Currently only the "Contribution" type of transactions are processed.
I will be adding support for distributions, sells, and other transactions.

Here are few examples:

```
,4/3/2012,Contribution,230,0,46,184,460
,4/17/2012,Contribution,230,0,46,184,460
TSPGF,4/17/2012,Contribution,138,0,138,13.8846,9.939
TSPGF,5/1/2012,Contribution,138,0,138,13.8946,9.932
TSPCF,4/17/2012,Contribution,161,0,161,17.2881,9.3128
TSPCF,5/1/2012,Contribution,161,0,161,17.4828,9.209
```


**Create an account for TSP in Quicken:**

If you dont have an account for TSP in Quicken, create one:


* Click on Quicken menu Tools -> Add Account
* Select 401 (k) or 403 (b) account type
* Select "Advanced Setup" as TSP will not be in the list of financial institutions
* Select "I want to enter transactions manually", then "Next"
* Give the account a name
* Select a date for Statement Ending. I used a date before I began working for the government.
* Select either "Current Employer" or "Previous Employer"
* Select "My Account" or "My Spouse's Account"
* Select "Yes" for Does your statement list hop many shares of each security you own
* Click "Next"
* Select "No" for tracking loans. I will add support for this in the future.
* Click "Next"
* Do not enter any securities and click "Next"
* Click "Yes" on the popup confirming to create account without securities
* Click "Done"
* It is optional to set up Paycheck tracking
* Click "Finish" and you should have an account ready for importing transactions.

 
**Running the program:**

The parser program is written in Python and tested with version 3.8.3 on 64 bit Windows 10 and Quicken Deluxe 2020 R26.23.

The syntax is as follows:

```
C:\Downloads>python tsp_csv_parser.py --help
usage: tsp_csv_parser.py [-h] [-infile INFILE] [--outfile OUTFILE]

optional arguments:
  -h, --help            show this help message and exit
  -infile INFILE, -i INFILE
                        set input filename
  --outfile OUTFILE, -o OUTFILE
                        set output filename
```

The infile parameter is required and should point to your csv file.

The outfile parameter is optional. If not provided, the path and filename of your csv file is used and the .csv is replaced with .qif

After a successful run of the program, you should have a .qif file.

 

**Importing into Quicken:**

To import the .qif file:


* In Quicken, Click on File -> File Import -> QIF File.
* In the popup dialog window
    * select the location of qif file using the Browse option
    * select the TSP account from the list of "Quicken account to import into:"
    * check all optionsin "Include in Import:"
* Click Import

Verify the imported transactions with your quarterly statement.

I usually check the total number of shares at the very end and if the totals match, all is well!

 